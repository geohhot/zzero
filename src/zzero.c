
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "easy_tcc.h"

struct statbar {
  int width;
  int done, all;
};

struct statbar init_statbar (int cs) {
  struct statbar tsb;
  tsb.width = cs;
  tsb.all = 1;
  tsb.done = 0;
  return tsb;
}

void draw_statbar (struct statbar *sb) {
    int width = sb->width;

    int done_count = (sb->done * width) / sb->all;
    int undone = width - done_count;

    putc ('[', stdout);
    while (done_count --)
      putc ('=', stdout);
    while (undone --)
      putc (' ', stdout);
    putc (']', stdout);

    fflush (stdout);
}

void start_statbar (struct statbar *sb, int done, int all) {
  sb->done = done;
  sb->all = all;
  draw_statbar (sb);
}

void update_statbar (struct statbar *sb, int done, int all) {
  // go back then draw
  //cn_backward (sb->width);
  printf ("\rZZeroing ...\t");
  sb->done = done;
  sb->all = all;
  
  draw_statbar (sb);
}

int main (int argc, char *argv[]) {

  struct statbar sb = init_statbar (30);
  
  for (uint32_t i=1; i < argc; ++i) {
    char fname[256];
    strcpy (fname, argv[i]);
  
    printf ("File: %s\n", fname);

    // check if file exists
    FILE *fd;
    fd = fopen (fname, "r");

    // (local-set-key (kbd "s-c") (lambda () (interactive) (save-buffer) ( compile "make all")))
    
    if (fd == NULL) {
      // file does not exist
      fprintf (stderr, "No file found.\n");
      continue;
    }

    // get file size
    struct stat st;
    fstat(fileno (fd), &st);
    int32_t fsize = st.st_size;

    printf ("Size: %u\n", fsize);
    printf ("ZZeroing ...\t");

    // print stuff

    start_statbar (&sb, 0, fsize);
    fclose (fd);
    fd = fopen (fname, "w");

    for (int32_t pos=0; pos < fsize; ++pos) {
      fputc ('\x00', fd);

      if (pos % 10 == 0) {
	update_statbar (&sb, pos, fsize);
	fflush (stdout);
      }
    }
    update_statbar (&sb, fsize, fsize);
    
    printf ("\n");
    fclose (fd);
  }
  
  return 0;
}
