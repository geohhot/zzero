
ZZero ..
========

Is a tool, written in C, which basically rewrites all files given as arguments with 0x00s.
The initial meaning was - for curropting files, so undelete tools wouldn't be able to get data back.
Although, the deletion functions have not been tested this far.

TODO:
-----
* Test whether this actually works.
* Implement argument parser
* Add options for random data and amount of rewrites.
* DO some low level stuff with hard drive sectors...
